package tholdi.tholdiapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Consultation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation);
        final Button button_res_encours = (Button) findViewById(R.id.button_res_encours);
        final Button button_res_termine = (Button) findViewById(R.id.button_res_termine);
        final Button button_res_demande = (Button) findViewById(R.id.button_res_demande);

        button_res_encours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Consultation.this, ReservationsEnCours.class);
                Consultation.this.startActivity(myIntent);
            }
        });

        button_res_termine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Consultation.this, ReservationsTerminees.class);
                Consultation.this.startActivity(myIntent);
            }
        });

        button_res_demande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Consultation.this, ReservationsDemandees.class);
                Consultation.this.startActivity(myIntent);
        }
        });




//        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
//        String url = "http://172.16.0.112/wsClient/Reservation";
//        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                int Id;
//                String Datereservation;
//                String Dateprevuestockage;
//                int Nbjoursdestockageprevu;
//                int Quantite;
//                String Etat;
//                int Numclient;
//
//                try {
//                    int length = response.getJSONArray(0).length();
//                    for (int i = 0; i < response.getJSONArray(0).length(); i++) {
//                        JSONArray innerArray = response.getJSONArray(0);
//                        JSONObject jo = innerArray.getJSONObject(i);
//                        Id = jo.getInt("Id");
//                        Datereservation = jo.getString("Datereservation");
//                        Dateprevuestockage = jo.getString("Dateprevuestockage");
//                        Nbjoursdestockageprevu = jo.getInt("Nbjoursdestockageprevu");
//                        Quantite = jo.getInt("Quantite");
//                        Etat = jo.getString("Etat");
//                        Numclient = jo.getInt("Numclient");
//
//                        if(Etat == "encours"){
//
//                        }
//
//
//
//                    }
//                }catch (JSONException e) {
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }){
//
//        @Override
//        public Map<String, String> getHeaders() throws AuthFailureError {
//            Map<String, String> params = new HashMap<String, String>();
//            params.put("Content-Type", "application/x-www-form-urlencoded");
//            return params;
//        }
//        };
//        requestQueue.add(request);

    }
}
