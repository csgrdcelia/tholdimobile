package tholdi.tholdiapp;

public class Reservation {

    private int Id;
    private String Datereservation;
    private String Dateprevuestockage;
    private int Nbjoursdestockageprevu;
    private int Quantite;
    private String Etat;
    private int Numclient;

    public Reservation(int Id, String Datereservation, String Dateprevuestockage, int Nbjoursdestockageprevu, int Quantite, String Etat, int Numclient){
        this.Id = Id;
        this.Datereservation = Datereservation;
        this.Dateprevuestockage = Dateprevuestockage;
        this.Nbjoursdestockageprevu = Nbjoursdestockageprevu;
        this.Quantite = Quantite;
        this.Etat = Etat;
        this.Numclient = Numclient;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDatereservation() {
        return Datereservation;
    }

    public void setDatereservation(String datereservation) {
        Datereservation = datereservation;
    }

    public String getDateprevuestockage() {
        return Dateprevuestockage;
    }

    public void setDateprevuestockage(String dateprevuestockage) {
        Dateprevuestockage = dateprevuestockage;
    }

    public int getNbjoursdestockageprevu() {
        return Nbjoursdestockageprevu;
    }

    public void setNbjoursdestockageprevu(int nbjoursdestockageprevu) {
        Nbjoursdestockageprevu = nbjoursdestockageprevu;
    }

    public int getQuantite() {
        return Quantite;
    }

    public void setQuantite(int quantite) {
        Quantite = quantite;
    }

    public String getEtat() {
        return Etat;
    }

    public void setEtat(String etat) {
        Etat = etat;
    }

    public int getNumclient() {
        return Numclient;
    }

    public void setNumclient(int numclient) {
        Numclient = numclient;
    }
}
