package tholdi.tholdiapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ccas on 21/04/2017.
 */

public class CustomAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    ArrayList<Reservation> myList = new ArrayList<Reservation>();
    Context context;

    // on passe le context afin d'obtenir un LayoutInflater pour utiliser notre
    // row_layout.xml
    // on passe les valeurs de notre à l'adapter
    public CustomAdapter(Context context, ArrayList<Reservation> myList) {
        this.myList = myList;
        this.context = context;
    }

    // retourne le nombre d'objet présent dans notre liste
    @Override
    public int getCount() {
        return myList.size();
    }

    // retourne un élément de notre liste en fonction de sa position
    @Override
    public Reservation getItem(int position) {
        return myList.get(position);
    }

    // retourne l'id d'un élément de notre liste en fonction de sa position
    @Override
    public long getItemId(int position) {
        return myList.indexOf(getItem(position));
    }

    // retourne la vue d'un élément de la liste
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder = null;

        // au premier appel ConvertView est null, on inflate notre layout
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = mInflater.inflate(R.layout.reservation_list, parent, false);

            // nous plaçons dans notre MyViewHolder les vues de notre layout
            mViewHolder = new MyViewHolder();
            mViewHolder.tv_Id_value = (TextView) convertView
                    .findViewById(R.id.tv_Id_value);
            mViewHolder.tv_Numclient_value = (TextView) convertView
                    .findViewById(R.id.tv_Numclient_value);
            mViewHolder.tv_Datereservation_value = (TextView) convertView
                    .findViewById(R.id.tv_Datereservation_value);
            mViewHolder.tv_Dateprevuestockage_value = (TextView) convertView
                    .findViewById(R.id.tv_Dateprevuestockage_value);
            mViewHolder.tv_Nbjoursdestockageprevu_value = (TextView) convertView
                    .findViewById(R.id.tv_Nbjoursdestockageprevu_value);
            mViewHolder.tv_Quantite_value = (TextView) convertView
                    .findViewById(R.id.tv_Quantite_value);

            // nous attribuons comme tag notre MyViewHolder à convertView
            convertView.setTag(mViewHolder);
        } else {
            // convertView n'est pas null, nous récupérons notre objet MyViewHolder
            // et évitons ainsi de devoir retrouver les vues à chaque appel de getView
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        // nous récupérons l'item de la liste demandé par getView
        Reservation listItem = (Reservation) getItem(position);

        // nous pouvons attribuer à nos vues les valeurs de l'élément de la liste

        mViewHolder.tv_Id_value.setText(Integer.toString(listItem.getId()));
        mViewHolder.tv_Numclient_value.setText(Integer.toString(listItem.getNumclient()));
        mViewHolder.tv_Datereservation_value.setText(listItem.getDatereservation());
        mViewHolder.tv_Dateprevuestockage_value.setText(listItem.getDateprevuestockage());
        mViewHolder.tv_Nbjoursdestockageprevu_value.setText(Integer.toString(listItem.getNbjoursdestockageprevu()));
        mViewHolder.tv_Quantite_value.setText(Integer.toString(listItem.getQuantite()));

        // nous retournos la vue de l'item demandé
        return convertView;
    }

    // MyViewHolder va nous permettre de ne pas devoir rechercher
    // les vues à chaque appel de getView, nous gagnons ainsi en performance
    private class MyViewHolder {
        TextView tv_Id_value, tv_Numclient_value, tv_Datereservation_value, tv_Dateprevuestockage_value, tv_Nbjoursdestockageprevu_value, tv_Quantite_value;
    }

    // nous affichons un Toast à chaque clic sur un item de la liste
    // nous récupérons l'objet grâce à sa position
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
//        Toast toast = Toast.makeText(context, "Item " + (position + 1) + ": "
//                + this.myList.get(position), Toast.LENGTH_SHORT);
//        toast.show();

    }

}