package tholdi.tholdiapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReservationsEnCours extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations_en_cours);
        final ListView lv_ReservationsEnCours = (ListView) findViewById(R.id.lv_Reservationsencours);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = "http://172.16.0.112/wsClient/Reservation";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int Id;
                String Datereservation;
                String Dateprevuestockage;
                int Nbjoursdestockageprevu;
                int Quantite;
                String Etat;
                int Numclient;

                try {
                    int length = response.getJSONArray(0).length();
                    List<Integer> IdList = new ArrayList<Integer>();
                    List<String> DatereservationList = new ArrayList<String>();
                    List<String> DateprevuestockageList = new ArrayList<String>();
                    List<Integer> NbjoursdestockageprevuList = new ArrayList<Integer>();
                    List<Integer> QuantiteList = new ArrayList<Integer>();
                    List<String> EtatList = new ArrayList<String>();
                    List<Integer> NumclientList = new ArrayList<Integer>();
                    for (int i = 0; i < response.getJSONArray(0).length(); i++) {
                        try {
                            JSONArray innerArray = response.getJSONArray(0);
                            JSONObject jo = innerArray.getJSONObject(i);
                            Id = jo.getInt("Id");
                            Datereservation = jo.getString("Datereservation");
                            Dateprevuestockage = jo.getString("Dateprevuestockage");
                            Nbjoursdestockageprevu = jo.getInt("Nbjoursdestockageprevu");
                            Quantite = jo.getInt("Quantite");
                            Etat = jo.getString("Etat");
                            Numclient = jo.getInt("Numclient");

                            if(Datereservation.length() > 10)
                                Datereservation = Datereservation.substring(0,10);
                            if(Dateprevuestockage.length() > 10)
                                Dateprevuestockage = Datereservation.substring(0,10);

//                            if(Dateprevuestockage == null)
//                                Dateprevuestockage = "null";

                            if(Etat.equals("encours")) {
                                IdList.add(Id);
                                DatereservationList.add(Datereservation);
                                DateprevuestockageList.add(Dateprevuestockage);
                                NbjoursdestockageprevuList.add(Nbjoursdestockageprevu);
                                QuantiteList.add(Quantite);
                                EtatList.add(Etat);
                                NumclientList.add(Numclient);
                            }


                        }
                        catch(Exception e){
                            String j = e.getMessage();
                        }

                    }

                    ArrayList<Reservation> myList = new ArrayList<Reservation>();

                    for (int i = 0; i < IdList.size(); i++) {
                        myList.add(new Reservation(IdList.get(i), DatereservationList.get(i), DateprevuestockageList.get(i), NbjoursdestockageprevuList.get(i), QuantiteList.get(i), EtatList.get(i), NumclientList.get(i)));
                    }
                    CustomAdapter adapter = new CustomAdapter(ReservationsEnCours.this, myList);
                    lv_ReservationsEnCours.setAdapter(adapter);
//                    lv_ReservationsEnCours.setOnItemClickListener(adapter);

                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                return params;
//            }
        };
        requestQueue.add(request);

    }
}
