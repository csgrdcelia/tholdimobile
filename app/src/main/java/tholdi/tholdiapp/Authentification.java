package tholdi.tholdiapp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Authentification extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);

        final EditText etLogin = (EditText) findViewById(R.id.id);
        final EditText etPassword = (EditText) findViewById(R.id.mdp);
        final Button btnValider = (Button) findViewById(R.id.btnValider);

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String login = etLogin.getText().toString();
                final String password = etPassword.getText().toString();

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                StringRequest sr = new StringRequest(Request.Method.POST, "http://172.16.0.112/wsClient/Connection", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject connected = null;
                        try {
                            connected = new JSONObject(response);
                            boolean state = connected.getBoolean("connected");
                            if (state) {
                                Intent myIntent = new Intent(Authentification.this, Consultation.class);
                                Authentification.this.startActivity(myIntent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String t = error.getMessage();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("login", login);
                        params.put("password", password);
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }
                };
                queue.add(sr);
            }
        });
    }
}

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authentification);
//
//        final EditText etLogin = (EditText) findViewById(R.id.id);
//        final EditText etPassword = (EditText) findViewById(R.id.mdp);
//        final Button btnValider = (Button) findViewById(R.id.btnValider);
//
//        btnValider.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                final String login = etLogin.getText().toString();
//                final String password = etPassword.getText().toString();
//
//                Response.Listener<String> responseListener = new Response.Listener<String>(){
//
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonResponse = new JSONObject(response);
//                            boolean success = jsonResponse.getBoolean("success");
//
//                            if (success) {
//                                Intent intent = new Intent(Authentification.this, Consultation.class);
//                                Authentification.this.startActivity(intent);
//                            } else {
//                                AlertDialog.Builder builder = new AlertDialog.Builder(Authentification.this);
//                                builder.setMessage("Login Failed")
//                                        .setNegativeButton("Retry", null)
//                                        .create()
//                                        .show();
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                };
//
//                AuthentificationRequest authentificationRequest = new AuthentificationRequest(login, password, responseListener);
//                RequestQueue queue = Volley.newRequestQueue(Authentification.this);
//                queue.add(authentificationRequest);
//            }
//        });
//    }
//}

//
//    private final String EXTRA_LOGIN = "login";
//    private final String EXTRA_PASSWORD = "password";
//
//    private EditText login;
//    private EditText password;
//    private Button btnValider;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authentification);
//
//        login = (EditText) findViewById(R.id.id);
//        password = (EditText) findViewById(R.id.mdp);
//
//        btnValider = (Button) findViewById(R.id.btnValider);
//
//        btnValider.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Authentification.this, Consultation.class);
//                intent.putExtra(EXTRA_LOGIN, login.getText().toString());
//                intent.putExtra(EXTRA_PASSWORD, password.getText().toString());
//                startActivity(intent);
//            }
//        });
//
//        // Instantiate the RequestQueue.
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = "http://172.16.0.112/wsClient/Reservation";
//
//        // Request a string response from the provided URL.
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // Display the first 500 characters of the response string.
//                        ((TextView) findViewById(R.id.textView)).setText("Response is: " + response.substring(0, 500));
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                ((TextView) findViewById(R.id.textView)).setText("That didn't work!");
//            }
//        });
//
//        // Add the request to the RequestQueue.
//        queue.add(stringRequest);
//    }
//}
    /*{
        protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("login", "tholdi");
        params.put("password", "tholdi");
        return params;
    };*/

