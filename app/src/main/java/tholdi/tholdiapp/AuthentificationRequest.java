package tholdi.tholdiapp;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ccas on 30/03/2017.
 */

public class AuthentificationRequest extends StringRequest {
    private static final String LOGIN_REQUEST_URL = "http://www.portdebarcelona.cat/wsClient/Connection";
    private Map<String, String> params;

    public AuthentificationRequest(String login, String password, Response.Listener<String> listener) {
        super(Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("login", login);
        params.put("password", password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }


}
